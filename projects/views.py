from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def list_projects(request):
    projects_list = Project.objects.filter(owner=request.user)
    context = {
        "projects_list": projects_list,
    }
    return render(request, "projects/list.html", context)


# Create your views here.


@login_required
def show_projects(request, id):
    show_projects = Project.objects.get(id=id)
    context = {
        "show_projects": show_projects,
    }

    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {"form": form, }
    return render(request, "projects/create.html", context)

# @login_required
# def show_form(request, id):
#     if request.method == "POST":
#         form = ShowForm(request.POST)
#         if form.is_valid():
#             project = form.save(False)
#             project.task = request.user
#             project.save()
#             return redirect("projects_lists")
#     else:
#         form = ShowForm()
#     context = {
#         "form": form}
#     return redirect()
